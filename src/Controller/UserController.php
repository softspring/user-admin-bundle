<?php

namespace Softspring\UserAdminBundle\Controller;

use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Util\TokenGenerator;
use Softspring\UserBundle\Model\User;
use Softspring\UserBundle\SfsUserEvents;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AbstractController
{
    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var TokenGenerator
     */
    protected $tokenGenerator;

    /**
     * UserController constructor.
     *
     * @param EventDispatcherInterface $dispatcher
     * @param TokenGenerator           $tokenGenerator
     */
    public function __construct(EventDispatcherInterface $dispatcher, TokenGenerator $tokenGenerator)
    {
        $this->dispatcher = $dispatcher;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @return Response
     */
    public function search()
    {
        /** @var EntityRepository $repo */
        $repo = $this->getDoctrine()->getRepository($this->getUserClass());
        $qb = $repo->createQueryBuilder('u');
        $qb->andWhere($qb->expr()->isNull('u.invitationToken'));

        $user = $qb->getQuery()->getResult();

        return $this->render('@SfsUserAdmin/user/search.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @return Response
     */
    public function invitations()
    {
        /** @var EntityRepository $repo */
        $repo = $this->getDoctrine()->getRepository($this->getUserClass());
        $qb = $repo->createQueryBuilder('u');
        $qb->andWhere($qb->expr()->isNotNull('u.invitationToken'));

        $user = $qb->getQuery()->getResult();

        return $this->render('@SfsUserAdmin/user/invitations.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function create(Request $request)
    {
        $userClass = $this->getUserClass();

        /** @var User $user */
        $user = new $userClass();
        $user->setPassword('invalid');
        $user->setEnabled(false);

        $form = $this->createForm($this->getParameter('sfs_user_admin.create_form'), $user, ['validation_groups' => ['Default', 'user-create']]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('sfs_user_admin_list');
        }

        return $this->render('@SfsUserAdmin/user/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function invite(Request $request)
    {
        $userClass = $this->getUserClass();

        /** @var User $user */
        $user = new $userClass();
        $user->setPassword('invalid');
        $user->setEnabled(true);

        $form = $this->createForm($this->getParameter('sfs_user_admin.invite_form'), $user, ['validation_groups' => ['Default', 'user-create']]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setEnabled(false);
            $user->setInvitationToken($this->tokenGenerator->generateToken());

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $event = new UserEvent($user, $request);
            $this->dispatcher->dispatch(SfsUserEvents::USER_INVITED, $event);

            return $this->redirectToRoute('sfs_user_admin_list');
        }

        return $this->render('@SfsUserAdmin/user/invite.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param         $user
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function update($user, Request $request)
    {
        $user = $this->getDoctrine()->getRepository($this->getUserClass())->findOneById($user);

        $form = $this->createForm($this->getParameter('sfs_user_admin.update_form'), $user, ['validation_groups' => ['Default', 'user-update']]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('sfs_user_admin_list');
        }

        return $this->render('@SfsUserAdmin/user/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param         $user
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function delete($user, Request $request)
    {
        $user = $this->getDoctrine()->getRepository($this->getUserClass())->findOneById($user);

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('sfs_user_admin_list');
    }

    /**
     * @return string
     */
    protected function getUserClass(): string
    {
        return $this->getParameter('fos_user.model.user.class');
    }
}