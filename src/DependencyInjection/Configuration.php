<?php

namespace Softspring\UserAdminBundle\DependencyInjection;

use Softspring\UserAdminBundle\Form\UserCreateForm;
use Softspring\UserAdminBundle\Form\UserInviteForm;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('sfs_user_admin');

        $rootNode
            ->children()
                ->scalarNode('create_form')->defaultValue(UserCreateForm::class)->end()
                ->scalarNode('invite_form')->defaultValue(UserInviteForm::class)->end()
                ->scalarNode('update_form')->defaultValue(UserCreateForm::class)->end()
            ->end();

        return $treeBuilder;
    }
}