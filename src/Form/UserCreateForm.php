<?php

namespace Softspring\UserAdminBundle\Form;

use Softspring\UserBundle\Model\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserCreateForm extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => 'sfs_user_admin',
            'label_format' => 'form.user.%name%.label',
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username');
        $builder->add('name');
        $builder->add('surname');
        $builder->add('email');
        $builder->add('roles', ChoiceType::class, [
            'expanded' => true,
            'multiple' => true,
            'choices' => [
                'ROLE_SUPER_ADMIN' => 'ROLE_SUPER_ADMIN',
                'ROLE_ADMIN' => 'ROLE_ADMIN',
            ],
        ]);
    }
}